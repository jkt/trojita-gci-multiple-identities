CONFIG += staticlib
TEMPLATE = lib
TARGET = Composer
SOURCES += \
    SubjectMangling.cpp \
    PlainTextFormatter.cpp
HEADERS += \
    SubjectMangling.h \
    PlainTextFormatter.h
